﻿using Holistor.Calculus.Core;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.Loader;
using System.Text.RegularExpressions;

namespace Holistor.Calculus.Services
{

    public static class EngineService
    {
        private static object lockerObj = new object();
        public static string Execute(ParametrosEngine parametros, string rootPath)
        {
            //var newDomain = AppDomain.CreateDomain("Mono" + Guid.NewGuid().ToString());
            //var assembly =  newDomain.Load(new AssemblyName("Holistor.Calculus.Core"));
            //var assemblyFormule   = newDomain.Load(new AssemblyName("Holistor.Calculus.Formules"));
            //var engine = assembly.CreateInstance("Holistor.Calculus.Core.Engine");
            //MethodInfo mi = engine.GetType().GetMethod("ExecuteEngine");
            //string result = (string)mi.Invoke(engine, new object[] { nameSpace, formulas, xml, pathResult, resultByFormule });
            //AppDomain.Unload(newDomain);
            //return result;

            lock (lockerObj)
            {
                /*   var exePath = Path.GetDirectoryName(System.Reflection
                                    .Assembly.GetExecutingAssembly().CodeBase);
                   Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*");
                   var basePath = appPathMatcher.Match(exePath).Value; ;*/


                if (parametros.ResultByFormule == true)
                {
                    var basePathResult = Path.Combine(rootPath, "results");
                    if (Directory.Exists(basePathResult) == false)
                        Directory.CreateDirectory(basePathResult);

                    parametros.PathResult = Path.Combine(basePathResult, parametros.PathResult);
                    if (!Directory.Exists(parametros.PathResult))
                    {
                        Directory.CreateDirectory(parametros.PathResult);
                    }
                    var basePathSteps = Path.Combine(parametros.PathResult, "Steps");
                    if (!Directory.Exists(basePathSteps))
                    {
                        Directory.CreateDirectory(basePathSteps);
                    }
                }
                var basePath = rootPath;
                var assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(Path.Combine(basePath, "Holistor.Calculus.Core.dll"));
                var context = AssemblyLoadContext.GetLoadContext(assembly);
                var assemblyFormule = context.LoadFromAssemblyPath(Path.Combine(basePath, "Holistor.Calculus.Formules.dll"));

                var engine = assembly.CreateInstance("Holistor.Calculus.Core.Engine");
                MethodInfo mi = engine.GetType().GetMethod("ExecuteEngine");
                string result = (string)mi.Invoke(engine, new object[] { parametros });

                return result;
            }
        }
    }
}