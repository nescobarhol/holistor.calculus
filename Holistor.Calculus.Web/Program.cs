﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Holistor.Calculus.Web
{
    public class Program
    {
         public static void Main(string[] args)
        {
           // BuildWebHost(args).Run();

            var config = new ConfigurationBuilder()
         .AddCommandLine(args)
         .AddEnvironmentVariables()
         .Build();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseConfiguration(config)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .Build();
            host.Run();
        }


    }
    
}
