﻿using System;
using System.Text;
using Holistor.Calculus.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;

namespace Holistor.Calculus.Web.Controllers
{
    [Route("api/engine")]
    [ApiController]
    public class EngineController : ControllerBase
    {

        public EngineController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        private readonly IHostingEnvironment _hostingEnvironment;
        [HttpPost]
        [Route("execute")]
        public void Execute(string cola,ParametrosEngine parametros )
        {
   

            var factory = new ConnectionFactory() { Uri = new Uri("amqps://biyxoavp:p4fEw-WjciiTCSb4PyQYYgae8aGrN-S1@prawn.rmq.cloudamqp.com/biyxoavp") } ; // , Port = 	1883 };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
      
                    channel.QueueDeclare(cola, false, false, false, null);
  
                    var body = Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(parametros));
                    channel.BasicPublish(String.Empty, cola, null, body);
                }
            }
        }
    }
}