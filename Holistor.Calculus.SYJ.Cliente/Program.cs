﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using Holistor.Calculus.Core;
using System.IO;

namespace Holistor.Calculus.SYJ.Cliente
{

    class Program
    {
        static void Main(string[] args)


        {
            if (args.Length != 4)
            {
                args = new string[4];
                args[0] = "n3";//"pocIn";
                args[1] = "n3out";//"pocOut";
                args[2] = "x";//"localhost";
                args[3] = "x";// "5672";
            }
            String colaIn = Convert.ToString(args[0]);
            String colaOut = Convert.ToString(args[1]);
         /* String host = Convert.ToString(args[2]);
            String puerto = Convert.ToString(args[3]); */

          //  var factory = new ConnectionFactory() { HostName = host, Port = Convert.ToInt32(puerto) };
            var factory = new ConnectionFactory() { Uri = new Uri("amqps://biyxoavp:p4fEw-WjciiTCSb4PyQYYgae8aGrN-S1@prawn.rmq.cloudamqp.com/biyxoavp") }; // , Port = 	1883 };

            using (var connection = factory.CreateConnection())
            {
                using (var channelIn = connection.CreateModel())
                {
                    using (var channelOut = connection.CreateModel())
                    {

                        channelIn.QueueDeclare(colaIn, false, false, false, null);
                        channelOut.QueueDeclare(colaOut, false, false, false, null);

                        var consumer = new QueueingBasicConsumer(channelIn);
                        channelIn.BasicConsume(colaIn, true, consumer);
                        Console.WriteLine("Esperando los mensajes...");
                        while (true)
                        {
                            var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
                            var bodyIn = ea.Body;
                            var msg = Encoding.UTF8.GetString(bodyIn);
                            var parametros = Newtonsoft.Json.JsonConvert.DeserializeObject<ParametrosEngine>(msg);

                            string rootPath =Environment.CurrentDirectory;
                            if (parametros.ResultByFormule == true)
                            {
                                var basePathResult = Path.Combine(rootPath, "results");
                                if (Directory.Exists(basePathResult) == false)
                                    Directory.CreateDirectory(basePathResult);

                                parametros.PathResult = Path.Combine(basePathResult, parametros.PathResult);
                                if (!Directory.Exists(parametros.PathResult))
                                {
                                    Directory.CreateDirectory(parametros.PathResult);
                                }
                                var basePathSteps = Path.Combine(parametros.PathResult, "Steps");
                                if (!Directory.Exists(basePathSteps))
                                {
                                    Directory.CreateDirectory(basePathSteps);
                                }
                            }

                            var engine = new Holistor.Calculus.Core.Engine();
                            string result = engine.ExecuteEngine(parametros);
                            var bodyOut = Encoding.UTF8.GetBytes(result);
                            channelOut.BasicPublish(String.Empty, colaOut, null, bodyOut);
                        }
                    }
                }
            }
        }
    }
}


