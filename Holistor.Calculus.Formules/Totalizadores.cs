﻿using Holistor.Calculus.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Holistor.Calculus.Formules
{
    public class Totalizadores : TotalizadorBase, ITotalizadores
    {

        public Decimal TotalUnidad1;
        public Decimal TotalUnidad2;
        public Decimal TotalUnidad3;

        public Decimal Total1;
        public Decimal Total2;
        public Decimal Total3;

        public Decimal ContadorOperaciones;
        public Decimal ContadorLecturas;

        public String Nombre;

        public String ProcesoEjecutado;

        public Totalizadores()
        {
         
        }
    }
}
