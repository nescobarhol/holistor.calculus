﻿using Holistor.Calculus.Core;

namespace Holistor.Calculus.Formules
{
    public partial class FormulaBase : FormulaCore
    {


        public static DsResultado Resultado
        {
            get
            {
                return (DsResultado)GetResultModel();
            }
        }

        public static DsOrigen Origen
        {
            get
            {
                return (DsOrigen)modelOrigen;
            }
        }

        public static DsOrigen.VariablesDataTable Variables
        {
            get
            {
                return (DsOrigen.VariablesDataTable)modelOrigen.Variables;
            }
        }

        private static DsOrigen modelOrigen;

        public static void IniciarBase()
        {
            modelOrigen = new DsOrigen();
            AddModel("Origen", modelOrigen, true);

            DsResultado modelResult = new DsResultado();
            SetResultModel(modelResult);

        }

        public static Totalizadores CrearTotalizadores()
        {

            Totalizadores tot = new Totalizadores();
            totalizadores = tot;

            return totalizadores;
        }

        private static Totalizadores totalizadores;

        protected static Totalizadores Totalizadores
        {
            get
            {
                return totalizadores;
            }
        }
    }
}
