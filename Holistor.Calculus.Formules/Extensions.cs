﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Holistor.Calculus.Formules
{
    public static class Extensions
    {
        #region Methods

        /// <summary>
        /// Extension Right.Devuelve el número especificado de caracteres del extremo derecho de una cadena de caracteres.
        /// </summary>
        /// <param name="Stg">Especifica la expresión de caracteres de la que se devuelven los caracteres del extremo derecho</param>
        /// <param name="nLarge">Especifica el número de caracteres devueltos desde la expresión de caracteres</param>
        /// <returns>String</returns>
        public static string Right(this System.String Stg, Int32 nLarge)
        {
            string resultado = "";

            if (nLarge >= Stg.Length)
            {
                resultado = Stg;
            }
            else if (nLarge < 0)
            {
                resultado = "";
            }
            else
            {
                resultado = Stg.Substring(Stg.Length - nLarge);
            }

            return resultado;
        }

        /// <summary>
        /// Extension Left.Devuelve un número especificado de caracteres de una expresión de caracteres, a partir del carácter situado más a la izquierda.
        /// </summary>
        /// <param name="Stg">Especifica la expresión de caracteres de la cual LEFT( ) devuelve los caracteres.</param>
        /// <param name="nLarge">Especifica el número de caracteres devueltos desde la expresión de caracteres</param>
        /// <returns>String</returns>
        public static string Left(this System.String Stg, Int32 nLarge)
        {
            string resultado = "";

            if (nLarge >= Stg.Length)
            {
                resultado = Stg;
            }
            else if (nLarge < 0)
            {
                resultado = "";
            }
            else
            {
                resultado = Stg.Substring(0, nLarge);
            }

            return resultado;
        }

        /// <summary>
        /// Devuelve una cadena de caracteres que contiene una expresión de caracteres especificada que se repite un determinado número de veces.
        /// </summary>
        /// <param name="Stg"></param>
        /// <param name="nTimes">Especifica el número de veces que se replica la expresión de caracteres.</param>
        /// <returns></returns>
        public static string Replicate(this System.String Stg, int nTimes)
        {
            return new StringBuilder().Insert(0, Stg, nTimes).ToString();
        }

        #endregion
    }
}
