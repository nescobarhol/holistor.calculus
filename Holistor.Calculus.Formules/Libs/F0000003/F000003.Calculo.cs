﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holistor.Calculus.Formules
{
    public partial class F000003Calculo : BaseCalculo
    {
        public override void Execute()
        {

            for (int i = 0; i < 10000; i++)
            {
                var variableParam = (from variable in Variables where variable.Nombre == "Parametro3" select variable).FirstOrDefault();
                Decimal parametro = variableParam.Valor;
                Totalizadores.ContadorLecturas += 1;
                Totalizadores.Total3 = Totalizadores.Total3 + parametro;
                Totalizadores.ContadorOperaciones += 1;

            }
        }
    }
}
