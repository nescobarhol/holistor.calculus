﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holistor.Calculus.Formules
{
    public partial class F000002Calculo : BaseCalculo
    {
        public override void Execute()
        {
     

            for (int i = 0; i < 1000; i++)
            {
                var variableParam = (from variable in Variables where variable.Nombre == "Parametro2" select variable).FirstOrDefault();
                Decimal parametro = variableParam.Valor;
                Totalizadores.ContadorLecturas += 1;
                Totalizadores.Total2 = Totalizadores.Total2 + parametro;
                Totalizadores.ContadorOperaciones += 1;
         
                var row = Resultado.Registros.NewRegistrosRow();
                row.Id = (int)Totalizadores.ContadorOperaciones;
                row.Mensaje = Guid.NewGuid().ToString();
                Resultado.Registros.AddRegistrosRow(row);

            }
        }
    }
}
