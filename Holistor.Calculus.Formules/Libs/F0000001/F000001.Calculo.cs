﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holistor.Calculus.Formules
{
    public partial class F000001Calculo : BaseCalculo
    {
        public override void Execute()
        {
            Totalizadores.ProcesoEjecutado = "Calculo";
            Totalizadores.Nombre = Origen.Suscripcion[0].ApellidoRSocial;
            for (int i = 0; i < 10000; i++)
            {
                var variableParam = (from variable in Variables where variable.Nombre == "Parametro1" select variable).FirstOrDefault();
                Decimal parametro = variableParam.Valor;
                Totalizadores.ContadorLecturas += 1;
                Importe = Importe + parametro;
                Cantidad = Cantidad + parametro / 2;
                Totalizadores.ContadorOperaciones += 1;
            }
        }
    }
}

