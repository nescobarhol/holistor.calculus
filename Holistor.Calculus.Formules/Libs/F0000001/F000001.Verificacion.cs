﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holistor.Calculus.Formules
{
    public partial class F000001Verificacion : BaseVerificacion
    {


        public override void Execute()
        {

            Totalizadores.ProcesoEjecutado = "Verificacion";

            for (int i = 0; i < 100; i++)
            {
                var variableParam = (from variable in Variables where variable.Nombre == "Parametro1" select variable).FirstOrDefault();
                Decimal parametro = variableParam.Valor;
                Totalizadores.ContadorLecturas += 1;
                Totalizadores.Total1 = Totalizadores.Total1 + parametro;
                Totalizadores.ContadorOperaciones += 1;

            }
        }
    }
}
